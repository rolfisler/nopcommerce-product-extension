﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Services.Blogs;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Web.Areas.Admin.Controllers;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Blogs;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Models.Extensions;
using Nop.Web.Framework.Mvc;
using VIU.Plugin.Extensions.Product.Admin.Model.RelatedBlogPost;
using VIU.Plugin.Extensions.Product.Admin.Services;
using VIU.Plugin.Extensions.Product.Core.Domain;
using VIU.Plugin.Extensions.Product.Core.Services;

namespace VIU.Plugin.Extensions.Product.Admin.Controller
{
    public sealed class ExtendedProductController : BaseAdminController
    {
        private const string RelatedBlogPostAddPopupView =
            "~/Plugins/VIU.Extensions.Product.Admin/Views/RelatedBlogPosts/RelatedBlogPostAddPopup.cshtml";

        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly IBlogService _blogService;
        private readonly IExtendedProductService _extendedProductService;
        private readonly IPermissionService _permissionService;
        private readonly IExtendedBlogService _extendedBlogService;
        private readonly IUrlRecordService _urlRecordService;

        public ExtendedProductController(
            IBaseAdminModelFactory baseAdminModelFactory, 
            IBlogService blogService,
            IExtendedProductService extendedProductService, 
            IPermissionService permissionService,
            IExtendedBlogService extendedBlogService, 
            IUrlRecordService urlRecordService)
        {
            _baseAdminModelFactory = baseAdminModelFactory;
            _blogService = blogService;
            _extendedProductService = extendedProductService;
            _permissionService = permissionService;
            _extendedBlogService = extendedBlogService;
            _urlRecordService = urlRecordService;
        }

        [HttpPost]
        public IActionResult RelatedBlogPostList(RelatedBlogPostSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedDataTablesJson();

            var relatedBlogPosts = _extendedProductService.GetRelatedBlogPostsByProductId(searchModel.ProductId,
                searchModel.Page - 1, searchModel.PageSize);

            var model = new RelatedBlogPostListModel().PrepareToGrid(searchModel, relatedBlogPosts, () =>
                relatedBlogPosts.Select(relatedBlogPost => new RelatedBlogPostModel
                {
                    Id = relatedBlogPost.Id,
                    RelatedBlogPostId = relatedBlogPost.BlogPostId,
                    Title = _blogService.GetBlogPostById(relatedBlogPost.BlogPostId).Title,
                    LanguageName = _blogService.GetBlogPostById(relatedBlogPost.BlogPostId).Language.Name,
                    DisplayOrder = relatedBlogPost.DisplayOrder
                }));

            return Json(model);
        }

        [HttpPost]
        public IActionResult RelatedBlogPostUpdate(RelatedBlogPostModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var relatedBlogPost = _extendedProductService.GetRelatedBlogPostById(model.Id)
                                  ?? throw new ArgumentException("No related blog post found with the specified id");

            relatedBlogPost.DisplayOrder = model.DisplayOrder;
            _extendedProductService.UpdateRelatedBlogPost(relatedBlogPost);

            return new NullJsonResult();
        }

        [HttpPost]
        public IActionResult RelatedBlogPostDelete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var relatedBlogPost = _extendedProductService.GetRelatedBlogPostById(id)
                                  ?? throw new ArgumentException("No related blog post found with the specified id");

            _extendedProductService.DeleteRelatedBlogPost(relatedBlogPost);

            return new NullJsonResult();
        }

        public IActionResult RelatedBlogPostAddPopup(int productId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var model = new AddRelatedBlogPostSearchModel();
            _baseAdminModelFactory.PrepareStores(model.AvailableStores);
            model.SetPopupGridPageSize();

            return View(RelatedBlogPostAddPopupView, model);
        }

        [HttpPost]
        public IActionResult RelatedBlogPostAddPopupList(AddRelatedBlogPostSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedDataTablesJson();

            var blogPosts = _extendedBlogService.SearchBlogPosts(
                storeId: searchModel.SearchStoreId,
                keywords: searchModel.SearchBlogTitle,
                pageIndex: searchModel.Page - 1,
                pageSize: searchModel.PageSize,
                showHidden: true);

            var model = new AddRelatedBlogPostListModel().PrepareToGrid(searchModel, blogPosts, () =>
            {
                return blogPosts.Select(blogPost =>
                {
                    var blogPostModel = blogPost.ToModel<BlogPostModel>();
                    blogPostModel.SeName = _urlRecordService.GetSeName(blogPost, 0, true, false);

                    return blogPostModel;
                });
            });

            return Json(model);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public IActionResult RelatedBlogPostAddPopup(AddRelatedBlogPostModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var selectedBlogPosts = _blogService.GetBlogPostsByIds(model.SelectedBlogPostIds.ToArray());
            if (selectedBlogPosts.Any())
            {
                var existingRelatedBlogPosts = _extendedProductService.GetRelatedBlogPostsByProductId(model.ProductId);
                foreach (var blogPost in selectedBlogPosts)
                {
                    if (existingRelatedBlogPosts.FirstOrDefault(relatedProduct =>
                            relatedProduct.ProductId == model.ProductId && relatedProduct.BlogPostId == blogPost.Id) !=
                        null)
                        continue;

                    _extendedProductService.InsertRelatedBlogPost(new RelatedBlogPost
                    {
                        ProductId = model.ProductId,
                        BlogPostId = blogPost.Id,
                        DisplayOrder = 1
                    });
                }
            }

            ViewBag.RefreshPage = true;

            return View(RelatedBlogPostAddPopupView, new AddRelatedBlogPostSearchModel());
        }
    }
}