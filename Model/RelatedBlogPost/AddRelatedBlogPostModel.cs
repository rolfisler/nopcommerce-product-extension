﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Areas.Admin.Models.Blogs;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace VIU.Plugin.Extensions.Product.Admin.Model.RelatedBlogPost
{
    public class AddRelatedBlogPostModel : BaseNopModel
    {
        public AddRelatedBlogPostModel()
        {
            SelectedBlogPostIds = new List<int>();
        }

        public int ProductId { get; set; }

        public IList<int> SelectedBlogPostIds { get; set; }
    }
    
    public class AddRelatedBlogPostSearchModel : BaseSearchModel
    {
        public AddRelatedBlogPostSearchModel()
        {
            AvailableStores = new List<SelectListItem>();
        }

        [NopResourceDisplayName("Plugins.VIU.Extensions.Product.RelatedBlogPosts.SearchBlogTitle")]
        public string SearchBlogTitle { get; set; }
            
        [NopResourceDisplayName("Admin.Catalog.Products.List.SearchStore")]
        public int SearchStoreId { get; set; }
        
        public IList<SelectListItem> AvailableStores { get; set; }
    }
    
    public class AddRelatedBlogPostListModel : BasePagedListModel<BlogPostModel> { }
}