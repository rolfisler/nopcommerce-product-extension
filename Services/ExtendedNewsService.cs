﻿using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.News;
using Nop.Core.Domain.Stores;
using Nop.Services.Events;
using Nop.Services.Logging;
using Nop.Services.News;

namespace VIU.Plugin.Extensions.Product.Admin.Services
{
    public class ExtendedNewsService : NewsService
    {
        private readonly ILogger _logger;

        public ExtendedNewsService(CatalogSettings catalogSettings, IEventPublisher eventPublisher, IRepository<NewsComment> newsCommentRepository, IRepository<NewsItem> newsItemRepository, IRepository<StoreMapping> storeMappingRepository, ILogger logger) : base(catalogSettings, eventPublisher, newsCommentRepository, newsItemRepository, storeMappingRepository)
        {
            _logger = logger;
        }
        
        public override NewsItem GetNewsById(int newsId)
        {
            _logger.Information("ExtendedNewsService was called");
            
            return base.GetNewsById(newsId);
        }
    }
}