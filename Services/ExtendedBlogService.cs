﻿using System;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Blogs;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Stores;
using Nop.Services.Blogs;
using Nop.Services.Events;

namespace VIU.Plugin.Extensions.Product.Admin.Services
{
    public class ExtendedBlogService : BlogService, IExtendedBlogService
    {
        private readonly CatalogSettings _catalogSettings;
        private readonly IRepository<BlogPost> _blogPostRepository;
        private readonly IRepository<StoreMapping> _storeMappingRepository;
        
        public ExtendedBlogService(CatalogSettings catalogSettings, IEventPublisher eventPublisher,
            IRepository<BlogComment> blogCommentRepository, IRepository<BlogPost> blogPostRepository,
            IRepository<StoreMapping> storeMappingRepository) : base(catalogSettings, eventPublisher,
            blogCommentRepository, blogPostRepository, storeMappingRepository)
        {
            _catalogSettings = catalogSettings;
            _blogPostRepository = blogPostRepository;
            _storeMappingRepository = storeMappingRepository;
        }

        public IPagedList<BlogPost> SearchBlogPosts(
            int storeId = 0, 
            int languageId = 0, 
            DateTime? dateFrom = null, 
            DateTime? dateTo = null,
            string titleKeywords = null, 
            int pageIndex = 0,
            int pageSize = Int32.MaxValue, 
            bool showHidden = false)
        {
            var query = _blogPostRepository.Table;
            if(!string.IsNullOrWhiteSpace(titleKeywords))
                query = query.Where(b => b.Title.Contains(titleKeywords, StringComparison.OrdinalIgnoreCase));
            if (dateFrom.HasValue)
                query = query.Where(b => dateFrom.Value <= (b.StartDateUtc ?? b.CreatedOnUtc));
            if (dateTo.HasValue)
                query = query.Where(b => dateTo.Value >= (b.StartDateUtc ?? b.CreatedOnUtc));
            if (languageId > 0)
                query = query.Where(b => languageId == b.LanguageId);
            if (!showHidden)
            {
                query = query.Where(b => !b.StartDateUtc.HasValue || b.StartDateUtc <= DateTime.UtcNow);
                query = query.Where(b => !b.EndDateUtc.HasValue || b.EndDateUtc >= DateTime.UtcNow);
            }

            if (storeId > 0 && !_catalogSettings.IgnoreStoreLimitations)
            {
                //Store mapping
                query = from bp in query
                    join sm in _storeMappingRepository.Table
                        on new { c1 = bp.Id, c2 = nameof(BlogPost) } equals new { c1 = sm.EntityId, c2 = sm.EntityName } into bp_sm
                    from sm in bp_sm.DefaultIfEmpty()
                    where !bp.LimitedToStores || storeId == sm.StoreId
                    select bp;

                query = query.Distinct();
            }

            query = query.OrderByDescending(b => b.StartDateUtc ?? b.CreatedOnUtc);

            var blogPosts = new PagedList<BlogPost>(query, pageIndex, pageSize);
            return blogPosts;
        }
    }
}