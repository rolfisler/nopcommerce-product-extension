﻿using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Blogs;

namespace VIU.Plugin.Extensions.Product.Admin.Services
{
    public interface IExtendedBlogService
    {
        IPagedList<BlogPost> SearchBlogPosts(
            int storeId = 0, 
            int languageId = 0,
            DateTime? dateFrom = null, 
            DateTime? dateTo = null,
            string keywords = null,
            int pageIndex = 0, 
            int pageSize = int.MaxValue, 
            bool showHidden = false);
    }
}