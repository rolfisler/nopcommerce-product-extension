using Autofac;
using Nop.Core.Configuration;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Services.News;
using VIU.Plugin.Extensions.Product.Admin.Services;

namespace VIU.Plugin.Extensions.Product.Admin.Infrastructure
{
    /// <summary>
    /// Dependency registrar
    /// </summary>
    public class DependencyRegistrar : IDependencyRegistrar
    {
        /// <summary>
        /// Register services and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        /// <param name="typeFinder">Type finder</param>
        /// <param name="config">Config</param>
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            builder.RegisterType<ExtendedBlogService>().As<IExtendedBlogService>().InstancePerLifetimeScope();

            builder.RegisterType<ExtendedNewsService>().As<INewsService>().InstancePerLifetimeScope();
            builder.RegisterType<NewsServiceExtension>().As<INewsService>().InstancePerLifetimeScope();
        }

        /// <summary>
        /// Order of this dependency registrar implementation
        /// </summary>
        public int Order => 9999;
    }
}